//
//  UIFont+ZRExtension.m
//  简书:https://www.jianshu.com/u/043e94ca450f
//
//  Created by 黄涛 on 2018/5/18.
//  Copyright © 2018年 zero. All rights reserved.
//

#import "UIFont+ZRExtension.h"

@implementation UIFont (ZRExtension)

/** 像素转字体 */
+ (UIFont *)zr_systemFontOfPxSize:(CGFloat)pxSize{
    
    return [self systemFontOfSize:pxSize * 3 / 4];
}



@end
