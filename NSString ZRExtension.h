//
//  NSString+ZRExtension.h
//  简书:https://www.jianshu.com/u/043e94ca450f
//
//  Created by 黄涛 on 2018/8/3.
//  Copyright © 2018年 zero. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ZRExtension)

#pragma mark - -----------------------  格式化 ----------------------
/** 去除首尾空格 */
- (NSString *)zr_delSpace;

/** 去除所有空格 注意:此时生成的str是autorelease属性的，不要妄想对strUrl进行release操作。 */
- (NSString *)zr_delAllSpace;


#pragma mark - ----------------------- 正则方法 ----------------------
/** 判断 手机号 */
- (BOOL)zr_isPhoneNum;

/** 判断身份证号 */
- (BOOL)zr_isIdCard;

/** 判断 邮箱 */
- (BOOL)zr_isEmail;

/** 判断 车牌号*/
- (BOOL)zr_isCarNo;

/** 判断 url */
- (BOOL)zr_isUrl;

/** 检查 用户名(1 - 15 位) */
- (BOOL)zr_checkAccount;

/** 判断 纯数字 */
- (BOOL)zr_isNumber;

/** 判断 纯字母 */
- (BOOL)zr_isEnglish;

/** 判断 纯汉字 */
- (BOOL)zr_isChinese;

/** 检查 密码(min - max 位) 数字.字母.下划线 组合 */
- (BOOL)zr_checkPasswordMin:(int)a max:(int)b;

/** 检查 银行卡号
 *  现行 16 位银联卡现行卡号开头 6 位是 622126～622925 之间的，7 到 15 位是银行自定义的，
 *  可能是发卡分行，发卡网点，发卡序号，第 16 位是校验码。
 *  16 位卡号校验位采用 Luhm 校验方法计算：
 *  1，将未带校验位的 15 位卡号从右依次编号 1 到 15，位于奇数位号上的数字乘以 2
 *  2，将奇位乘积的个十位全部相加，再加上所有偶数位上的数字
 *  3，将加法和加上校验位能被 10 整除。
 */
- (BOOL)zr_checkBankCard;

#pragma mark - ----------------------- 计算尺寸 ----------------------
/** 计算文字 宽度 font:字号 maxH:最大高度 */
- (CGFloat)zr_calculateWidthWithFont:(CGFloat)font maxHeight:(CGFloat)maxH;

/** 计算文字 宽度 font:字号 maxW:最大宽度 */
- (CGFloat)zr_calculateHeightWithFont:(CGFloat)font maxWidth:(CGFloat)maxW;

#pragma mark - ----------------------- 散列函数 ----------------------
/** MD5 加密 */
- (NSString *)zr_md5String;

/** SHA1 加密 :可修改参数更换 加密 如:SHA512 */
- (NSString *)zr_sha1String;
@end
