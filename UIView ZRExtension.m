//
//  UIView+ZRExtension.m
//  简书:https://www.jianshu.com/u/043e94ca450f
//
//  Created by 黄涛 on 2018/5/17.
//  Copyright © 2018年 zero. All rights reserved.
//
#import "UIView+ZRExtension.h"

@implementation UIView (ZRExtension)

/** 尺寸 */
- (CGSize)size{return self.frame.size;}

- (void)setSize:(CGSize)size{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

/** 宽度 */
- (CGFloat)width{return self.frame.size.width;}

- (void)setWidth:(CGFloat)width{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

/** 高度 */
- (CGFloat)height{return self.frame.size.height;}

- (void)setHeight:(CGFloat)height{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

/** origin */
- (CGPoint)origin{return self.frame.origin;}

- (void)setOrigin:(CGPoint)origin{
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

/** X */
- (CGFloat)x{return self.frame.origin.x;}

- (void)setX:(CGFloat)x{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

/** Y */
- (CGFloat)y{return self.frame.origin.y;}

- (void)setY:(CGFloat)y{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

/** centerX */
- (CGFloat)centerX{return self.center.x;}

- (void)setCenterX:(CGFloat)centerX{
    CGPoint center = self.center;
    center.x = centerX;
    self.center = center;
}

/** centerY */
- (CGFloat)centerY{return self.center.y;}

- (void)setCenterY:(CGFloat)centerY{
    CGPoint center = self.center;
    center.y = centerY;
    self.center = center;
}

/** maxX */
- (CGFloat)maxX{return CGRectGetMaxX(self.frame);}
/** maxY */
- (CGFloat)maxY{return CGRectGetMaxY(self.frame);}
/** midX */
- (CGFloat)midX{return CGRectGetMidX(self.frame);}
/** midY */
- (CGFloat)midY{return CGRectGetMidY(self.frame);}

/** 添加一条线 */
- (void)zr_addLineWithFrame:(CGRect)frame color:(UIColor *)color{
    
    CALayer * line = [CALayer layer];
    line.frame = frame;
    line.backgroundColor = color.CGColor;
    [self.layer addSublayer:line];
}

/** 截取 指定 大小 */
- (UIImage *)zr_screenShotsWithRect:(CGRect)rect{
    
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, [UIScreen mainScreen].scale);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    if (context == NULL) return nil;
    CGContextSaveGState(context);
    CGContextTranslateCTM(context, -rect.origin.x, -rect.origin.y);
    
    //    [self respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)]?
    //    [self drawViewHierarchyInRect:rect afterScreenUpdates:NO]:
    [self.layer.presentationLayer renderInContext:context];
    
    CGContextRestoreGState(context);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    return image;
}

/** 截取 全屏 */
- (UIImage *)zr_screenShots{
    
    CGRect screenRect = [UIScreen mainScreen].bounds;
    UIGraphicsBeginImageContext(screenRect.size);
    UIGraphicsGetCurrentContext();
    [self.layer.presentationLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    return image;
}

@end
