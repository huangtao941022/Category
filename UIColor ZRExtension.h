//
//  UIColor+ZRExtension.h
//  简书:https://www.jianshu.com/u/043e94ca450f
//
//  Created by 黄涛 on 2018/7/12.
//  Copyright © 2018年 zero. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ZRExtension)

/** 16 进制转 UIcolor 例:[#FFFFFF -> 白色] */
+ (UIColor *)zr_colorWithHexString:(NSString *)hexString;

/** 16 进制 32位 转 UIcolor 例:[0xFF0000 -> 红色] */
+ (UIColor *)zr_colorWithHexNum:(uint32_t)hexNum;

/** 纯色图片 */
- (UIImage *)zr_colorImageWithWidth:(CGFloat)width height:(CGFloat)height cornerRadius: (CGFloat)cornerRadius;
@end
