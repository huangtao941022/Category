//
//  UIView+ZRExtension.h
////  简书:https://www.jianshu.com/u/043e94ca450f
//
//  Created by 黄涛 on 2018/5/17.
//  Copyright © 2018年 zero. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ZRExtension)

/** 尺寸 */
@property (nonatomic, assign) CGSize size;
/** 宽度 */
@property (nonatomic, assign) CGFloat width;
/** 高度 */
@property (nonatomic, assign) CGFloat height;
/** origin */
@property (nonatomic, assign) CGPoint origin;
/** X */
@property (nonatomic, assign) CGFloat x;
/** Y */
@property (nonatomic, assign) CGFloat y;
/** centerX */
@property (nonatomic, assign) CGFloat centerX;
/** centerY */
@property (nonatomic, assign) CGFloat centerY;
/** maxX */
@property (nonatomic, assign , readonly) CGFloat maxX;
/** maxY */
@property (nonatomic, assign , readonly) CGFloat maxY;
/** midX */
@property (nonatomic, assign , readonly) CGFloat midX;
/** midY */
@property (nonatomic, assign , readonly) CGFloat midY;


/** 添加一条线 */
- (void)zr_addLineWithFrame:(CGRect)frame color:(UIColor *)color;

/** 截取 全屏 */
- (UIImage *)zr_screenShots;

/** 截取 指定 大小 */
- (UIImage *)zr_screenShotsWithRect:(CGRect)rect;

@end
